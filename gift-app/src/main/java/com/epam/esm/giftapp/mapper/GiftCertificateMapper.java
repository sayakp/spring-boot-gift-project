package com.epam.esm.giftapp.mapper;

import com.epam.esm.giftapp.domain.GiftCertificate;
import com.epam.esm.giftapp.dto.GiftCertificateDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GiftCertificateMapper {

    GiftCertificate toGiftCertificate(GiftCertificateDto giftCertificateDto);

    GiftCertificateDto fromGiftCertificate(GiftCertificate giftCertificate);
}
