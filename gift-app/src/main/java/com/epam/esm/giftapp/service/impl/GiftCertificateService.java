package com.epam.esm.giftapp.service.impl;

import com.epam.esm.giftapp.domain.GiftCertificate;
import com.epam.esm.giftapp.domain.Tag;
import com.epam.esm.giftapp.dto.GiftCertificateDto;
import com.epam.esm.giftapp.dto.TagDto;
import com.epam.esm.giftapp.exception.NoFieldsToUpdateException;
import com.epam.esm.giftapp.exception.ResourceNotFoundException;
import com.epam.esm.giftapp.mapper.GiftCertificateMapper;
import com.epam.esm.giftapp.mapper.TagMapper;
import com.epam.esm.giftapp.repository.GiftCertificateRepository;
import com.epam.esm.giftapp.repository.TagRepository;
import com.epam.esm.giftapp.service.CrudService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GiftCertificateService implements CrudService<GiftCertificateDto, Long> {

    GiftCertificateRepository giftCertificateRepository;
    TagRepository tagRepository;
    GiftCertificateMapper giftCertificateMapper;
    TagMapper tagMapper;

    public GiftCertificateService(GiftCertificateRepository giftCertificateRepository,
                                  GiftCertificateMapper giftCertificateMapper,
                                  TagRepository tagRepository,
                                  TagMapper tagMapper) {
        this.giftCertificateRepository = giftCertificateRepository;
        this.tagRepository = tagRepository;
        this.giftCertificateMapper = giftCertificateMapper;
        this.tagMapper = tagMapper;
    }

    @Override
    public List<GiftCertificateDto> getAll() {
        return giftCertificateRepository.findAll().stream()
                .map(giftCertificateMapper::fromGiftCertificate)
                .collect(Collectors.toList());
    }

    @Override
    public GiftCertificateDto get(Long id) {
        GiftCertificate giftCertificate = getCertificateById(id);
        return giftCertificateMapper.fromGiftCertificate(giftCertificate);
    }

    @Transactional
    @Override
    public GiftCertificateDto add(GiftCertificateDto giftCertificateDto) {
        // Clear id in case it's passed in the body
        giftCertificateDto.setId(null);

        GiftCertificate giftCertificateToSave = giftCertificateMapper.toGiftCertificate(giftCertificateDto);
        Date currentDate = Date.from(Instant.now());
        giftCertificateToSave.setCreateDate(currentDate);
        giftCertificateToSave.setLastUpdateDate(currentDate);

        if (giftCertificateToSave.getTags() != null) {
            giftCertificateToSave.setTags(updateTags(giftCertificateDto.getTags()));
        } else {
            giftCertificateToSave.setTags(new ArrayList<>());
        }

        return giftCertificateMapper.fromGiftCertificate(giftCertificateRepository.save(giftCertificateToSave));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        GiftCertificate giftCertificateToDelete = giftCertificateRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Cannot delete. GiftCertificate not found with id: " + id));

        giftCertificateToDelete.getTags().clear();

        giftCertificateRepository.delete(giftCertificateToDelete);
    }

    @Transactional
    @Override
    public GiftCertificateDto update(GiftCertificateDto giftCertificateDto) {
        // Check if the certificate exists and obtain it if it does
        GiftCertificate existingGiftCertificate = getCertificateById(giftCertificateDto.getId());
        // Check if there are no fields to update
        if (giftCertificateDto.getName() == null &&
                giftCertificateDto.getDescription() == null &&
                giftCertificateDto.getDuration() == null &&
                giftCertificateDto.getPrice() == null &&
                giftCertificateDto.getTags() == null) {
            throw new NoFieldsToUpdateException("No fields to update");
        }

        if (giftCertificateDto.getName() != null) {
            existingGiftCertificate.setName(giftCertificateDto.getName());
        }
        if (giftCertificateDto.getDescription() != null) {
            existingGiftCertificate.setDescription(giftCertificateDto.getDescription());
        }
        if (giftCertificateDto.getDuration() != null) {
            existingGiftCertificate.setDuration(giftCertificateDto.getDuration());
        }
        if (giftCertificateDto.getPrice() != null) {
            existingGiftCertificate.setPrice(giftCertificateDto.getPrice());
        }

        // Update Tags
        if (giftCertificateDto.getTags() != null) {
            existingGiftCertificate.setTags(updateTags(giftCertificateDto.getTags()));
        }

        existingGiftCertificate.setLastUpdateDate(Date.from(Instant.now()));

        return giftCertificateMapper.fromGiftCertificate(giftCertificateRepository.save(existingGiftCertificate));
    }

    private List<Tag> updateTags(List<TagDto> tags) {
        // Find if there are new tags to create
        return tags.stream()
                .map(tagDto -> tagRepository.findFirstByName(tagDto.getName())
                        .orElseGet(() -> {
                            Tag newTag = new Tag();
                            newTag.setName(tagDto.getName());
                            return tagRepository.save(newTag);
                        }))
                .collect(Collectors.toList());
    }

    private GiftCertificate getCertificateById(Long id) {
        return giftCertificateRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("GiftCertificate not found with id: " + id)
        );
    }
}
