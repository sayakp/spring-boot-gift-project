package com.epam.esm.giftapp.controller;

import com.epam.esm.giftapp.dto.TagDto;
import com.epam.esm.giftapp.service.impl.TagService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {

    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping()
    public List<TagDto> getAllTags() {
        return tagService.getAll();
    }

    @GetMapping("/{id}")
    public TagDto getTagById(@PathVariable Long id) {
        return tagService.get(id);
    }

    @PostMapping()
    public TagDto saveTag(@Valid @RequestBody TagDto tagRequestDto) {
        return tagService.add(tagRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteTagById(@PathVariable Long id) {
        tagService.delete(id);
    }
}
