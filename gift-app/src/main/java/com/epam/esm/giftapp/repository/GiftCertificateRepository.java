package com.epam.esm.giftapp.repository;

import com.epam.esm.giftapp.domain.GiftCertificate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GiftCertificateRepository extends JpaRepository<GiftCertificate, Long> {
}
