package com.epam.esm.giftapp.service;

import java.util.List;

public interface CrdService<DTO, IdType> {
    List<DTO> getAll();

    DTO get(IdType id);

    DTO add(DTO dto);

    void delete(IdType id);
}
