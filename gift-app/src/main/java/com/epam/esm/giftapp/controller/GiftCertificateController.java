package com.epam.esm.giftapp.controller;

import com.epam.esm.giftapp.dto.GiftCertificateDto;
import com.epam.esm.giftapp.service.impl.GiftCertificateService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/gift-certificate")
public class GiftCertificateController {

    private final GiftCertificateService giftCertificateService;

    public GiftCertificateController(GiftCertificateService giftCertificateService) {
        this.giftCertificateService = giftCertificateService;
    }

    @GetMapping()
    public ResponseEntity<List<GiftCertificateDto>> getAllGiftCertificates() {
        return ResponseEntity.ok(giftCertificateService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<GiftCertificateDto> getGiftCertificateById(@PathVariable Long id) {
        return ResponseEntity.ok(giftCertificateService.get(id));
    }

    @PostMapping()
    public ResponseEntity<GiftCertificateDto> saveGiftCertificate(@Valid @RequestBody GiftCertificateDto giftCertificateDto) {
        return ResponseEntity.ok(giftCertificateService.add(giftCertificateDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<GiftCertificateDto> updateGiftCertificate(@RequestBody GiftCertificateDto giftCertificateDto, @PathVariable Long id) {
        giftCertificateDto.setId(id);
        return ResponseEntity.ok(giftCertificateService.update(giftCertificateDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGiftCertificateById(@PathVariable Long id) {
        giftCertificateService.delete(id);
        return ResponseEntity.ok().build();
    }

}
