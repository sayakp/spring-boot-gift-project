package com.epam.esm.giftapp.dto;

import jakarta.validation.constraints.NotNull;

public class TagDto {
    private long id;
    @NotNull(message = "Name cannot be null")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
