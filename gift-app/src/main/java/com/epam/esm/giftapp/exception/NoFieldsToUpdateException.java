package com.epam.esm.giftapp.exception;

public class NoFieldsToUpdateException extends RuntimeException {
    public NoFieldsToUpdateException(String message) {
        super(message);
    }
}
