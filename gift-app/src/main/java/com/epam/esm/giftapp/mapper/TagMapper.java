package com.epam.esm.giftapp.mapper;

import com.epam.esm.giftapp.domain.Tag;
import com.epam.esm.giftapp.dto.TagDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TagMapper {
    Tag toTag(TagDto tagDto);

    TagDto fromTag(Tag tag);
}
