package com.epam.esm.giftapp.service.impl;

import com.epam.esm.giftapp.domain.Tag;
import com.epam.esm.giftapp.dto.TagDto;
import com.epam.esm.giftapp.exception.ResourceNotFoundException;
import com.epam.esm.giftapp.mapper.TagMapper;
import com.epam.esm.giftapp.repository.TagRepository;
import com.epam.esm.giftapp.service.CrdService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagService implements CrdService<TagDto, Long> {
    TagRepository tagRepository;
    TagMapper tagMapper;


    public TagService(TagRepository tagRepository,
                      TagMapper tagMapper) {
        this.tagRepository = tagRepository;
        this.tagMapper = tagMapper;
    }

    @Override
    public List<TagDto> getAll() {
        return tagRepository.findAll().stream().map(tagMapper::fromTag).collect(Collectors.toList());
    }

    @Override
    public TagDto get(Long id) {
        Tag tag = tagRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Tag not found with id: " + id));
        return tagMapper.fromTag(tag);
    }

    @Override
    public TagDto add(TagDto tagDto) {
        Tag tag = tagMapper.toTag(tagDto);
        tag = tagRepository.save(tag);
        return tagMapper.fromTag(tag);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Tag tagToDelete = tagRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Cannot delete. Tag not found with id: " + id));

        // Delete all relationships
        tagToDelete.getGiftCertificates().forEach(giftCertificate -> giftCertificate.getTags().remove(tagToDelete));
        tagRepository.delete(tagToDelete);
    }
}
