package com.epam.esm.giftapp.repository;

import com.epam.esm.giftapp.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Optional<Tag> findFirstByName(String name);
}
