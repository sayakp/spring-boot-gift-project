CREATE TABLE gift_certificate
(
    id               SERIAL PRIMARY KEY,
    name             VARCHAR(255)   NOT NULL NOT NULL,
    description      TEXT,
    price            NUMERIC(12, 2) NOT NULL,
    duration         INTEGER        NOT NULL,
    create_date      TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    last_update_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE tag
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE gift_certificate_tag
(
    gift_certificate_id INTEGER REFERENCES gift_certificate (id),
    tag_id              INTEGER REFERENCES tag (id),
    PRIMARY KEY (gift_certificate_id, tag_id)
);

INSERT INTO gift_certificate (name, description, price, duration)
VALUES ('Gift Certificate 1', 'Description 1', 10.99, 30),
       ('Gift Certificate 2', 'Description 2', 15.99, 60),
       ('Gift Certificate 3', 'Description 3', 20.99, 90);

INSERT INTO tag (name)
VALUES ('Tag 1'),
       ('Tag 2'),
       ('Tag 3');

INSERT INTO gift_certificate_tag (gift_certificate_id, tag_id)
VALUES (1, 1),
       (1, 2),
       (2, 2),
       (3, 1),
       (3, 3);